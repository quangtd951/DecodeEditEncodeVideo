package com.quangtd.decodeeditencodevideo;

import android.net.Uri;

import com.quangtd.decodeeditencodevideo.effect.GrayScaleEffect;
import com.quangtd.decodeeditencodevideo.effect.InverseEffect;
import com.quangtd.decodeeditencodevideo.effect.SepiaEffect;

import org.m4m.IVideoEffect;
import org.m4m.MediaComposer;
import org.m4m.domain.FileSegment;

import java.io.File;
import java.io.IOException;

/**
 * QuangTD on 10/23/2017.
 */

public class ActivityVideoEffect extends ComposerTranscodeCoreActivity {
    private int effectIndex;

    @Override
    protected void getActivityInputs() {
        srcMediaName1 = "/storage/emulated/0/DCIM/Camera/VID_20171019_150730.mp4";
        dstMediaPath = "/storage/emulated/0/output.mp4";
        mediaUri1 = new org.m4m.Uri(Uri.fromFile(new File(srcMediaName1)).toString());
        effectIndex = 2;//gray scale
    }

    @Override
    protected void setTranscodeParameters(MediaComposer mediaComposer) throws IOException {
        mediaComposer.addSourceFile(mediaUri1);
        mediaComposer.setTargetFile(dstMediaPath);

        configureVideoEncoder(mediaComposer, videoWidthOut, videoHeightOut);
        configureAudioEncoder(mediaComposer);

        configureVideoEffect(mediaComposer);
    }

    private void configureVideoEffect(MediaComposer mediaComposer) {
        IVideoEffect effect = null;
        switch (effectIndex) {
            case 0:
                effect = new SepiaEffect(0, factory.getEglUtil());
                break;
            case 1:
                effect = new GrayScaleEffect(0, factory.getEglUtil());
                break;
            case 2:
                effect = new InverseEffect(0, factory.getEglUtil());
                break;
            default:
                break;
        }

        if (effect != null) {
            effect.setSegment(new FileSegment(0l, 0l)); // Apply to the entire stream
            mediaComposer.addVideoEffect(effect);
        }
    }

    @Override
    protected void printEffectDetails() {
        effectDetails.append(String.format("Video effect = %s\n", getVideoEffectName(effectIndex)));
    }

    private String getVideoEffectName(int videoEffectIndex) {
        switch (videoEffectIndex) {
            case 0:
                return "Sepia";
            case 1:
                return "Grayscale";
            case 2:
                return "Inverse";
            case 3:
                return "Text Overlay";
            default:
                return "Unknown";
        }
    }
}
