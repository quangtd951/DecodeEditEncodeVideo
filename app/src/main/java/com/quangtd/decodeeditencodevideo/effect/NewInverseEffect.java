package com.quangtd.decodeeditencodevideo.effect;

import org.m4m.domain.graphics.IEglUtil;

/**
 * QuangTD on 10/25/2017.
 */

public class NewInverseEffect extends VideoEffect {
    public NewInverseEffect(int angle, IEglUtil eglUtil) {
        super(angle, eglUtil);
        setFragmentShader(getFragmentShader());
    }

    protected String getFragmentShader() {
       /* return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +
                "varying vec2 vTextureCoord;\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "void main() {\n" +
                "  vec4 color = texture2D(sTexture, vTextureCoord);\n" +
                "  float colorR = (1.0 - color.r) / 1.0;\n" +
                "  float colorG = (1.0 - color.g) / 1.0;\n" +
                "  float colorB = (1.0 - color.b) / 1.0;\n" +
                "  gl_FragColor = vec4(colorR, colorG, colorB, color.a);\n" +
                "}\n";*/
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;" +
                "varying vec2 vTextureCoord;" +
                "uniform samplerExternalOES sTexture;\n" +
                "void main() {" +
                "lowp vec4 color = texture2D(sTexture, vTextureCoord);" +
                "gl_FragColor = vec4((1.0 - color.rgb), color.w);" +
                "}";

    }
}
