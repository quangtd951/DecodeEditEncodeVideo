package com.quangtd.decodeeditencodevideo.effect;

import android.opengl.GLES20;

import org.m4m.domain.graphics.IEglUtil;

/**
 * QuangTD on 10/25/2017.
 */

public class NewShapeEffect extends VideoEffect {
    public NewShapeEffect(int angle, IEglUtil eglUtil) {
        super(angle, eglUtil);
        super.setVertexShader(VERTEX_SHADER);
        setFragmentShader(getFragmentShader());
    }

    protected String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision highp float;" +
                "uniform lowp samplerExternalOES sTexture;" +
                "varying highp vec2 textureCoordinate;" +
                "varying highp vec2 leftTextureCoordinate;" +
                "varying highp vec2 rightTextureCoordinate;" +
                "varying highp vec2 topTextureCoordinate;" +
                "varying highp vec2 bottomTextureCoordinate;" +
                "varying float centerMultiplier;" +
                "varying float edgeMultiplier;" +
                "void main() {" +
                "mediump vec3 textureColor       = texture2D(sTexture, textureCoordinate).rgb;" +
                "mediump vec3 leftTextureColor   = texture2D(sTexture, leftTextureCoordinate).rgb;" +
                "mediump vec3 rightTextureColor  = texture2D(sTexture, rightTextureCoordinate).rgb;" +
                "mediump vec3 topTextureColor    = texture2D(sTexture, topTextureCoordinate).rgb;" +
                "mediump vec3 bottomTextureColor = texture2D(sTexture, bottomTextureCoordinate).rgb;" +
                "gl_FragColor = vec4((textureColor * centerMultiplier - (leftTextureColor * edgeMultiplier + rightTextureColor * edgeMultiplier + topTextureColor * edgeMultiplier + bottomTextureColor * edgeMultiplier)), texture2D(sTexture, bottomTextureCoordinate).w);" +
                "}";

    }

    private float imageWidthFactor = 0.004f;
    private float imageHeightFactor = 0.004f;
    private float sharpness = 1.f;


    @Override
    protected void addEffectSpecific() {
        GLES20.glUniform1f(getHandle("imageWidthFactor"), imageWidthFactor);
        GLES20.glUniform1f(getHandle("imageHeightFactor"), imageHeightFactor);
        GLES20.glUniform1f(getHandle("sharpness"), sharpness);
    }

    private static final String VERTEX_SHADER =
            "attribute vec4 aPosition;" +
                    "uniform mat4 uMVPMatrix;\n" +
                    "uniform mat4 uSTMatrix;\n" +
                    "varying vec2 vTextureCoord;\n" +
                    "attribute vec4 aTextureCoord;" +
                    "uniform float imageWidthFactor;" +
                    "uniform float imageHeightFactor;" +
                    "uniform float sharpness;" +
                    "varying highp vec2 textureCoordinate;" +
                    "varying highp vec2 leftTextureCoordinate;" +
                    "varying highp vec2 rightTextureCoordinate;" +
                    "varying highp vec2 topTextureCoordinate;" +
                    "varying highp vec2 bottomTextureCoordinate;" +
                    "varying float centerMultiplier;" +
                    "varying float edgeMultiplier;" +
                    "void main() {" +
                    "   gl_Position = uMVPMatrix * aPosition;\n" +
                    "   vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n" +
                    "   mediump vec2 widthStep = vec2(imageWidthFactor, 0.0);" +
                    "   mediump vec2 heightStep = vec2(0.0, imageHeightFactor);" +
                    "   textureCoordinate       = aTextureCoord.xy;" +
                    "   leftTextureCoordinate   = textureCoordinate - widthStep;" +
                    "   rightTextureCoordinate  = textureCoordinate + widthStep;" +
                    "   topTextureCoordinate    = textureCoordinate + heightStep;" +
                    "   bottomTextureCoordinate = textureCoordinate - heightStep;" +
                    "   centerMultiplier = 1.0 + 4.0 * sharpness;" +
                    "   edgeMultiplier = sharpness;" +
                    "}";
}
