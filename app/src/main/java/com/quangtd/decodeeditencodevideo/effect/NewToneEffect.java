package com.quangtd.decodeeditencodevideo.effect;

import org.m4m.domain.graphics.IEglUtil;

/**
 * QuangTD on 10/25/2017.
 */

public class NewToneEffect extends VideoEffect {
    public NewToneEffect(int angle, IEglUtil eglUtil) {
        super(angle, eglUtil);
        setFragmentShader(getFragmentShader());
    }

    protected String getFragmentShader() {
        /*return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +
                "varying vec2 vTextureCoord;\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "void main() {\n" +
                "  vec4 color = texture2D(sTexture, vTextureCoord);\n" +
                "  float colorR = (1.0 - color.r) / 1.0;\n" +
                "  float colorG = (1.0 - color.g) / 1.0;\n" +
                "  float colorB = (1.0 - color.b) / 1.0;\n" +
                "  gl_FragColor = vec4(colorR, colorG, colorB, color.a);\n" +
                "}\n";*/
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +
                " varying highp vec2 vTextureCoord;\n" +
                " uniform lowp samplerExternalOES sTexture;\n" +
                " uniform mediump samplerExternalOES toneCurveTexture;\n" +
                "\n" +
                " void main()\n" +
                " {\n" +
                "     lowp vec4 textureColor = texture2D(sTexture, vTextureCoord);\n" +
                "     lowp float redCurveValue = texture2D(toneCurveTexture, vec2(textureColor.r, 3.0)).r;\n" +
                "     lowp float greenCurveValue = texture2D(toneCurveTexture, vec2(textureColor.g, 25.0)).g;\n" +
                "     lowp float blueCurveValue = texture2D(toneCurveTexture, vec2(textureColor.b, 100.0)).b;\n" +
                "\n" +
                "     gl_FragColor = vec4(redCurveValue, greenCurveValue, blueCurveValue, textureColor.a);\n" +
                " }";

    }

    private float threshold = 0.2f;
    private float quantizationLevels = 10f;

    protected int thresholdHandle;
    protected int quantizationLevelsHandle;

    @Override
    public void start() {
        super.start();
//        thresholdHandle = shaderProgram.getAttributeLocation("threshold");
//        quantizationLevelsHandle = shaderProgram.getAttributeLocation("quantizationLevels");
    }

    @Override
    protected void addEffectSpecific() {
//        GLES20.glUniform1f(thresholdHandle, threshold);
//        GLES20.glUniform1f(quantizationLevelsHandle, quantizationLevels);
    }
}
