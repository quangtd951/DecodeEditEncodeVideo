/*
 * Copyright 2014-2016 Media for Mobile
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quangtd.decodeeditencodevideo.effect;

import android.opengl.GLES20;

import org.m4m.domain.graphics.IEglUtil;

public class NewHazeEffect extends com.quangtd.decodeeditencodevideo.effect.VideoEffect {

    public NewHazeEffect(int angle, IEglUtil eglUtil) {
        super(angle, eglUtil);
        setFragmentShader(getFragmentShader());
    }

    protected String getFragmentShader() {
        return "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;" +
                "varying highp vec2 vTextureCoord;" +
                "uniform lowp samplerExternalOES sTexture;" +
                "uniform lowp float distance;" +
                "uniform highp float slope;" +
                "void main() {" +
                "highp vec4 color = vec4(1.0);" +
                "highp float  d = vTextureCoord.y * slope  +  distance;" +
                "highp vec4 c = texture2D(sTexture, vTextureCoord);" +
                "c = (c - d * color) / (1.0 -d);" +
                "gl_FragColor = c;" +    // consider using premultiply(c);
                "}";
    }

    private float distance = 0.2f;
    private float slope = 0.0f;

    @Override protected void addEffectSpecific() {
        super.addEffectSpecific();
        GLES20.glUniform1f(getHandle("distance"), distance);
        GLES20.glUniform1f(getHandle("slope"), slope);
    }
}
