package com.quangtd.decodeeditencodevideo.effect;

import android.opengl.GLES20;

import org.m4m.domain.graphics.IEglUtil;

/**
 * QuangTD on 10/25/2017.
 */

public class NewVignetteEffect extends VideoEffect {
    public NewVignetteEffect(int angle, IEglUtil eglUtil) {
        super(angle, eglUtil);
        setFragmentShader(getFragmentShader());
    }

    protected String getFragmentShader() {
        return
                "#extension GL_OES_EGL_image_external : require\n" +
                        "precision mediump float;" +
                        "varying vec2 vTextureCoord;" +
                        "uniform lowp samplerExternalOES sTexture;" +
                        "uniform lowp vec2 vignetteCenter;" +
                        "uniform lowp vec3 vignetteColor;" +
                        "uniform highp float vignetteStart;" +
                        "uniform highp float vignetteEnd;" +

                        "void main() {" +
                        "lowp vec3 rgb = texture2D(sTexture, vTextureCoord).rgb;" +
                        "lowp float d = distance(vTextureCoord, vec2(vignetteCenter.x, vignetteCenter.y));" +
                        "lowp float percent = smoothstep(vignetteStart, vignetteEnd, d);" +
                        "gl_FragColor = vec4(mix(rgb.x, vignetteColor.x, percent), mix(rgb.y, vignetteColor.y, percent), mix(rgb.z, vignetteColor.z, percent), 1.0);" +
                        "}";
    }

    private float vignetteCenterX = 0.5f;
    private float vignetteCenterY = 0.5f;
    private float[] vignetteColor = new float[]{0f, 0f, 0f};
    private float vignetteStart = 0.2f;
    private float vignetteEnd = 0.85f;

    @Override protected void addEffectSpecific() {
        super.addEffectSpecific();
        GLES20.glUniform2f(getHandle("vignetteCenter"), vignetteCenterX, vignetteCenterY);
        GLES20.glUniform3fv(getHandle("vignetteColor"), 0, vignetteColor, 0);
        GLES20.glUniform1f(getHandle("vignetteStart"), vignetteStart);
        GLES20.glUniform1f(getHandle("vignetteEnd"), vignetteEnd);
    }
}
