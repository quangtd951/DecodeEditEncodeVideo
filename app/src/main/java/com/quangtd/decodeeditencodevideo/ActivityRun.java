package com.quangtd.decodeeditencodevideo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaCodecInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.util.Matrix;
import com.quangtd.decodeeditencodevideo.effect.AddBlendEffect;
import com.quangtd.decodeeditencodevideo.effect.BlurEffectRS;
import com.quangtd.decodeeditencodevideo.effect.GrayScaleEffect;
import com.quangtd.decodeeditencodevideo.effect.InverseEffect;
import com.quangtd.decodeeditencodevideo.effect.JpegSubstituteEffect;
import com.quangtd.decodeeditencodevideo.effect.NewHazeEffect;
import com.quangtd.decodeeditencodevideo.effect.NewInverseEffect;
import com.quangtd.decodeeditencodevideo.effect.NewMonoChromeEffect;
import com.quangtd.decodeeditencodevideo.effect.NewShapeEffect;
import com.quangtd.decodeeditencodevideo.effect.NewToneEffect;
import com.quangtd.decodeeditencodevideo.effect.NewVignetteEffect;
import com.quangtd.decodeeditencodevideo.effect.RotateEffect;
import com.quangtd.decodeeditencodevideo.effect.SepiaEffect;
import com.quangtd.decodeeditencodevideo.effect.TextOverlayEffect;

import org.m4m.AudioFormat;
import org.m4m.IVideoEffect;
import org.m4m.MediaComposer;
import org.m4m.MediaFileInfo;
import org.m4m.android.AndroidMediaObjectFactory;
import org.m4m.android.AudioFormatAndroid;
import org.m4m.android.VideoFormatAndroid;
import org.m4m.domain.FileSegment;
import org.m4m.domain.Resolution;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * QuangTD on 10/24/2017.
 */

public class ActivityRun extends AppCompatActivity {
    protected AndroidMediaObjectFactory factory;
    protected MediaComposer mediaComposer;

    protected final String videoMimeType = "video/avc";
    protected int videoBitRateInKBytes = 5000;
    protected final int videoFrameRate = 30;

    protected final int videoIFrameInterval = 1;
    // Audio
    protected final String audioMimeType = "audio/mp4a-latm";

    protected int videoWidthOut = 1920;
    protected int videoHeightOut = 1080;

    protected final int audioBitRate = 96 * 1024;
    protected int videoWidthIn = 640;
    protected int videoHeightIn = 480;

    protected String srcMediaName1 = null;
    protected String srcMediaName2 = null;
    protected String dstMediaPath = null;
    protected org.m4m.Uri mediaUri1;
    protected org.m4m.Uri mediaUri2;
    protected ProgressBar progressBar;
    private boolean isStopped = false;

    protected org.m4m.AudioFormat audioFormat = null;
    protected org.m4m.VideoFormat videoFormat = null;
    protected long duration = 0;


    public org.m4m.IProgressListener progressListener = new org.m4m.IProgressListener() {
        @Override
        public void onMediaStart() {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(0);
                        updateUI(true);
                    }
                });
            } catch (Exception e) {
            }
        }

        @Override
        public void onMediaProgress(float progress) {

            final float mediaProgress = progress;

            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress((int) (progressBar.getMax() * mediaProgress));
                    }
                });
            } catch (Exception e) {
            }
        }

        @Override
        public void onMediaDone() {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isStopped) {
                            return;
                        }
                        updateUI(false);
                        reportTranscodeDone();
                    }
                });
            } catch (Exception e) {
            }
        }

        @Override
        public void onMediaPause() {
        }

        @Override
        public void onMediaStop() {
        }

        @Override
        public void onError(Exception exception) {
            try {
                final Exception e = exception;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateUI(false);
                        String message = (e.getMessage() != null) ? e.getLocalizedMessage() : e.toString();
                        showMessageBox("Transcoding failed." + "\n" + message, null);
                    }
                });
            } catch (Exception e) {
            }
        }
    };
    private Button buttonStart;
    private Button buttonStop;
    private MediaFileInfo mediaFileInfo;
    private VideoView videoView;
    private int mRotation;

    private void updateUI(boolean inProgress) {
        buttonStart.setEnabled(!inProgress);
        buttonStop.setEnabled(inProgress);

        if (inProgress) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private void reportTranscodeDone() {

        String message = "Transcoding finished.";

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                progressBar.setVisibility(View.INVISIBLE);
                findViewById(R.id.btnStart).setVisibility(View.GONE);
                findViewById(R.id.btnStop).setVisibility(View.GONE);
                if (mRotation != 0) {
                    try {
                        changeRotation(dstMediaPath, mRotation);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    playResult(dstMediaPath);
                }
            }
        };
        showMessageBox(message, listener);
    }

    private void playResult(String media) {

        String videoUrl = "file:///" + media;

        Intent intent = new Intent(Intent.ACTION_VIEW);

        Uri data = Uri.parse(videoUrl);
        intent.setDataAndType(data, "video/mp4");
        startActivity(intent);
    }

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);
        try {
            FFmpeg.getInstance(this).loadBinary(new FFmpegLoadBinaryResponseHandler() {
                @Override public void onFailure() {
                    Log.e("TAGG", "failure");
                }

                @Override public void onSuccess() {
                    Log.e("TAGG", "success");
                }

                @Override public void onStart() {

                }

                @Override public void onFinish() {

                }
            });
        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
        buttonStart = (Button) findViewById(R.id.btnStart);
        buttonStop = (Button) findViewById(R.id.btnStop);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(100);
        videoView = (VideoView) findViewById(R.id.videoView);
//        srcMediaName1 = "/storage/emulated/0/DCIM/Camera/test2.mp4";
        srcMediaName1 = "/storage/emulated/0/DCIM/Camera/testt.mp4";
//        srcMediaName1 = "/storage/emulated/0/test3.mp4";
        dstMediaPath = "/storage/emulated/0/output" + System.currentTimeMillis() + ".mp4";
        mediaUri1 = new org.m4m.Uri(Uri.fromFile(new File(srcMediaName1)).toString());
        videoView.setVideoPath(srcMediaName1);
        videoView.start();
        getFileInfo();
        updateUI(false);
        buttonStart.callOnClick();
    }

    protected void getFileInfo() {
        try {
            mediaFileInfo = new MediaFileInfo(new AndroidMediaObjectFactory(getApplicationContext()));
            mediaFileInfo.setUri(mediaUri1);

            duration = mediaFileInfo.getDurationInMicroSec();

            mRotation = mediaFileInfo.getRotation();
            audioFormat = (AudioFormat) mediaFileInfo.getAudioFormat();
            if (audioFormat == null) {
                showMessageBox("Audio format info unavailable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
            }

            videoFormat = (org.m4m.VideoFormat) mediaFileInfo.getVideoFormat();
            if (videoFormat == null) {
                showMessageBox("Video format info unavailable", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
            } else {
                videoWidthIn = videoFormat.getVideoFrameSize().width();
                videoHeightIn = videoFormat.getVideoFrameSize().height();
            }
        } catch (Exception e) {
            String message = (e.getMessage() != null) ? e.getMessage() : e.toString();

            showMessageBox(message, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
        }
    }

    public void startTranscode(View view) {
        try {
            transcode();

        } catch (Exception e) {
            view.setEnabled(false);
            String message = (e.getMessage() != null) ? e.getMessage() : e.toString();
            showMessageBox(message, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
        }
    }

    private void transcode() throws IOException {
        factory = new AndroidMediaObjectFactory(getApplicationContext());
        mediaComposer = new org.m4m.MediaComposer(factory, progressListener);
        setTranscodeParameters(mediaComposer);
        mediaComposer.start();
    }

    protected void configureVideoEncoder(org.m4m.MediaComposer mediaComposer, int width, int height) {

        VideoFormatAndroid videoFormat = new VideoFormatAndroid(videoMimeType, width, height);
        videoFormat.setVideoBitRateInKBytes(videoBitRateInKBytes);
        videoFormat.setVideoFrameRate(videoFrameRate);
        videoFormat.setVideoIFrameInterval(videoIFrameInterval);

        mediaComposer.setTargetVideoFormat(videoFormat);
    }

    private void setTranscodeParameters(MediaComposer mediaComposer) throws IOException {
        mediaComposer.addSourceFile(mediaUri1);
        mediaComposer.setTargetFile(dstMediaPath);

        configureVideoEncoder(mediaComposer, videoWidthOut, videoHeightOut);
        configureAudioEncoder(mediaComposer);
        configureVideoEffect(mediaComposer);
    }

    protected void configureAudioEncoder(org.m4m.MediaComposer mediaComposer) {

        AudioFormatAndroid aFormat = new AudioFormatAndroid(audioMimeType, audioFormat.getAudioSampleRateInHz(), audioFormat.getAudioChannelCount());

        aFormat.setAudioBitrateInBytes(audioBitRate);
        aFormat.setAudioProfile(MediaCodecInfo.CodecProfileLevel.AACObjectLC);

        mediaComposer.setTargetAudioFormat(aFormat);
    }

    private void configureVideoEffect(MediaComposer mediaComposer) {
        IVideoEffect effect = null;
        int effectIndex = 0;
        switch (effectIndex) {
            case 0:
                effect = new SepiaEffect(0, factory.getEglUtil());
                break;
            case 1:
                effect = new GrayScaleEffect(0, factory.getEglUtil());
                break;
            case 2:
                effect = new InverseEffect(0, factory.getEglUtil());
                break;
            case 3:
                effect = new TextOverlayEffect(0, factory.getEglUtil());
                break;
            case 4:
                effect = new JpegSubstituteEffect("/storage/emulated/0/PICTURES/Timehop/testoverlay.jpg", new Resolution(227, 59), 0, factory.getEglUtil());
                break;
            case 5:
                effect = new BlurEffectRS(0, this, factory.getEglUtil(), 25);
                break;
            case 6:
                effect = new AddBlendEffect(10, factory.getEglUtil());
                break;
            case 7:
                effect = new NewInverseEffect(0, factory.getEglUtil());
                break;
            case 8:
                effect = new NewToneEffect(0, factory.getEglUtil());
                break;
            case 9:
                effect = new NewMonoChromeEffect(0, factory.getEglUtil());
                break;
            case 10:
                effect = new NewVignetteEffect(0, factory.getEglUtil());
                break;
            case 11:
                effect = new NewHazeEffect(0, factory.getEglUtil());
                break;
            case 12:
                effect = new NewShapeEffect(0, factory.getEglUtil());
                break;
            case 13:
                effect = new RotateEffect(90, factory.getEglUtil());
                break;
            default:
                break;
        }

        effect.setSegment(new FileSegment(0l, 0l)); // Apply to the entire stream
        mediaComposer.addVideoEffect(effect);
    }


    public void stopTranscode(View view) {
        mediaComposer.stop();
    }

    public void showMessageBox(String message, DialogInterface.OnClickListener listener) {

        if (message == null) {
            message = "";
        }

        if (listener == null) {
            listener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            };
        }

        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setMessage(message);
        b.setPositiveButton("OK", listener);
        AlertDialog d = b.show();

        ((TextView) d.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
    }

    public void rotate(Context context, final String videoPath, int rotate) {
        FFmpeg fFmpeg = FFmpeg.getInstance(context);
        String cmdTemp = "-i \"%s\" -c copy -metadata:s:v:0 rotate=%d %s";
        final String output = dstMediaPath = "/storage/emulated/0/output_rotate" + System.currentTimeMillis() + ".mp4";
        String cmd = String.format(Locale.US, cmdTemp, videoPath, rotate, output);
        Log.e("TAGG", cmd);
        try {
            fFmpeg.execute(buildCommand(cmd), new FFmpegExecuteResponseHandler() {
                @Override public void onSuccess(String message) {
                    Log.e("TAGG", message);
                    playResult(output);
                }

                @Override public void onProgress(String message) {
                    Log.e("TAGG", message);
                }

                @Override public void onFailure(String message) {
                    Log.e("TAGG", message);
                }

                @Override public void onStart() {
                    Log.e("TAGG", "on start");
                }

                @Override public void onFinish() {
                    Log.e("TAGG", "on finish");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

    private String[] buildCommand(String cmd) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(cmd);
        while (m.find()) list.add(m.group(1).replace("\"", ""));
        String[] cmds = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            cmds[i] = list.get(i);
        }
        return cmds;
    }

    private void changeRotation(String inputPath, int rotation) throws IOException {
        /*IsoFile isoFile = new IsoFile(inputPath);
        MovieHeaderBox mvhd = Path.getPath(isoFile, "moov/mvhd");
        mvhd.setMatrix(Matrix.ROTATE_90);
        String outputPath = "/storage/emulated/0/output_rotation_mp4parser" + System.currentTimeMillis() + ".mp4";
        isoFile.writeContainer(new FileOutputStream(outputPath).getChannel());
        playResult(outputPath);*/
/*
        Movie result = MovieCreator.build(inputPath);
        // do something with the file
        Container out = new DefaultMp4Builder().build(result);
        MovieHeaderBox mvhd = Path.getPath(out, "moov/mvhd");
        mvhd.setMatrix(Matrix.ROTATE_90);
        String outputPath = "/storage/emulated/0/output_rotation_mp4parser" + System.currentTimeMillis() + ".mp4";
        out.writeContainer(new FileOutputStream(outputPath).getChannel());*/

        Movie inMovie = MovieCreator.build(inputPath);
        inMovie.setMatrix(Matrix.ROTATE_90);
        String outputPath = "/storage/emulated/0/output_rotation_mp4parser" + System.currentTimeMillis() + ".mp4";
        new DefaultMp4Builder().build(inMovie).writeContainer(new FileOutputStream(outputPath).getChannel());
        playResult(outputPath);
        Toast.makeText(this, outputPath, Toast.LENGTH_LONG).show();
    }
}
